/*
 *  SPDX-FileCopyrightText: 2019 Marco Martin <mart@kde.org>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents
import org.kde.draganddrop 2.0 as DragDrop

import org.kde.plasma.private.mobilehomescreencomponents 0.1 as HomeScreenComponents
import org.kde.plasma.private.containmentlayoutmanager 1.0 as ContainmentLayoutManager 

import org.kde.plasma.private.mobileshell 1.0 as MobileShell

FocusScope {
    id: root
    width: 640
    height: 480

    property Item toolBox

//BEGIN functions

    function recalculateMaxFavoriteCount() {
        if (!componentComplete) {
            return;
        }

        HomeScreenComponents.ApplicationListModel.maxFavoriteCount = Math.max(4, Math.floor(Math.min(width, height) / homeScreenContents.appletsLayout.cellWidth));
    }

//END functions

    property bool componentComplete: false
    onWidthChanged: recalculateMaxFavoriteCount()
    onHeightChanged:recalculateMaxFavoriteCount()
    Component.onCompleted: {
        HomeScreenComponents.ApplicationListModel.applet = plasmoid;
        HomeScreenComponents.ApplicationListModel.loadApplications();

        if (plasmoid.screen == 0) {
            MobileShell.HomeScreenControls.homeScreen = root
            MobileShell.HomeScreenControls.homeScreenWindow = root.Window.window
        }
        componentComplete = true;
        recalculateMaxFavoriteCount()
    }

    Plasmoid.onScreenChanged: {
        if (plasmoid.screen == 0) {
            MobileShell.HomeScreenControls.homeScreen = root
            MobileShell.HomeScreenControls.homeScreenWindow = root.Window.window
        }
    }
    Window.onWindowChanged: {
        if (plasmoid.screen == 0) {
            MobileShell.HomeScreenControls.homeScreenWindow = root.Window.window
        }
    }

    Connections {
        property real lastRequestedPosition: 0
        target: MobileShell.HomeScreenControls
        function onResetHomeScreenPosition() {
            mainFlickable.scrollToPage(0);
        }
        function onRequestRelativeScroll(pos) {
            lastRequestedPosition = pos.y;
        }
    }

    HomeScreenComponents.FlickablePages {
        id: mainFlickable

        anchors {
            fill: parent
            topMargin: plasmoid.availableScreenRect.y
            bottomMargin: plasmoid.screenGeometry.height - plasmoid.availableScreenRect.height - plasmoid.availableScreenRect.y
        }

        //TODO: favorite strip disappearing with everything else
        footer: favoriteStrip
        appletsLayout: homeScreenContents.appletsLayout

        contentWidth: Math.max(width, width * Math.ceil(homeScreenContents.itemsBoundingRect.width/width)) + (homeScreenContents.launcherDragManager.active ? width : 0)
        showAddPageIndicator: homeScreenContents.launcherDragManager.active

        dragGestureEnabled: root.focus && !appletsLayout.editMode && !plasmoid.editMode && !homeScreenContents.launcherDragManager.active

        HomeScreenComponents.HomeScreenContents {
            id: homeScreenContents
            width: mainFlickable.width * 100
            favoriteStrip: favoriteStrip
            launcherModel: HomeScreenComponents.ApplicationListModel

            PlasmaComponents.Label {
                id: metrics
                text: "M\nM"
                visible: false
                font.pointSize: theme.defaultFont.pointSize * 0.9
            }

            launcherDelegate: HomeScreenComponents.HomeDelegate {
                id: delegate
                width: homeScreenContents.launcherRepeater.cellWidth
                height: Math.min(parent.height, homeScreenContents.launcherRepeater.cellHeight)
                appletsLayout: homeScreenContents.appletsLayout

                //just the normal inline binding in height: fails as it gets broken, make it explicit
                Binding {
                    target: delegate
                    property: "height"
                    value: Math.min(delegate.parent.height, homeScreenContents.launcherRepeater.cellHeight)
                }
                parent: parentFromLocation
                reservedSpaceForLabel: metrics.height
                property Item parentFromLocation: {
                    switch (model.applicationLocation) {
                    case HomeScreenComponents.ApplicationListModel.Favorites:
                        return favoriteStrip.flow;
                    case HomeScreenComponents.ApplicationListModel.Desktop:
                    default:
                        return appletsLayout;
                    }
                }
                Component.onCompleted: {
                    if (model.applicationLocation !== HomeScreenComponents.ApplicationListModel.Favorites) {
                        delegate.x = mainFlickable.width;
                        appletsLayout.restoreItem(delegate);
                    }
                }

                onUserDrag: {
                    dragCenterX = dragCenter.x;
                    dragCenterY = dragCenter.y;
                    homeScreenContents.launcherDragManager.dragItem(delegate, dragCenter.x, dragCenter.y);

                    delegate.width = appletsLayout.cellWidth;
                    delegate.height = appletsLayout.cellHeight;

                    var pos = plasmoid.fullRepresentationItem.mapFromItem(delegate, dragCenter.x, dragCenter.y);

                    //SCROLL LEFT
                    if (pos.x < units.gridUnit) {
                        homeScreenContents.launcherRepeater.scrollLeftRequested();
                    //SCROLL RIGHT
                    } else if (pos.x > mainFlickable.width - units.gridUnit) {
                        homeScreenContents.launcherRepeater.scrollRightRequested();
                    //DON't SCROLL
                    } else {
                        homeScreenContents.launcherRepeater.stopScrollRequested();
                    }
                }

                onDragActiveChanged: {
                    homeScreenContents.launcherDragManager.active = dragActive
                    if (dragActive) {
                        // Must be 0, 0 as at this point dragCenterX and dragCenterY are on the drag before"
                        homeScreenContents.launcherDragManager.startDrag(delegate);
                        homeScreenContents.launcherDragManager.currentlyDraggedDelegate = delegate;
                    } else {
                        homeScreenContents.launcherDragManager.dropItem(delegate, dragCenterX, dragCenterY);
                        plasmoid.editMode = false;
                        editMode = false;
                        homeScreenContents.launcherRepeater.stopScrollRequested();
                        homeScreenContents.launcherDragManager.currentlyDraggedDelegate = null;
                        forceActiveFocus();
                    }
                }

                onLaunch: (x, y, icon, title) => {
                    if (icon !== "") {
                        print(delegate.iconItem)
                        NanoShell.StartupFeedback.open(
                                icon,
                                title,
                                delegate.iconItem.Kirigami.ScenePosition.x + delegate.iconItem.width/2,
                                delegate.iconItem.Kirigami.ScenePosition.y + delegate.iconItem.height/2,
                                Math.min(delegate.iconItem.width, delegate.iconItem.height));
                    }
                    root.launched();
                }
                onParentFromLocationChanged: {
                    if (!homeScreenContents.launcherDragManager.active && parent != parentFromLocation) {
                        parent = parentFromLocation;
                        if (model.applicationLocation === HomeScreenComponents.ApplicationListModel.Favorites) {
                            plasmoid.nativeInterface.stackBefore(delegate, parentFromLocation.children[index]);

                        } else if (model.applicationLocation === HomeScreenComponents.ApplicationListModel.Grid) {
                            plasmoid.nativeInterface.stackBefore(delegate, parentFromLocation.children[Math.max(0, index - HomeScreenComponents.ApplicationListModel.favoriteCount)]);
                        }
                    }
                }
            }
        }
    }

    HomeScreenComponents.FavoriteStrip {
        id: favoriteStrip

        appletsLayout: homeScreenContents.appletsLayout

        visible: flow.children.length > 0 || homeScreenContents.launcherDragManager.active || homeScreenContents.containsDrag

        opacity: homeScreenContents.launcherDragManager.active && HomeScreenComponents.ApplicationListModel.favoriteCount >= HomeScreenComponents.ApplicationListModel.maxFavoriteCount ? 0.3 : 1

        TapHandler {
            target: favoriteStrip
            onTapped: {
                //Hides icons close button
                homeScreenContents.appletsLayout.appletsLayoutInteracted();
                homeScreenContents.appletsLayout.editMode = false;
            }
            onLongPressed: homeScreenContents.appletsLayout.editMode = true;
            onPressedChanged: root.focus = true;
        }
    }
}

